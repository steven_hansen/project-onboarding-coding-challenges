package com.revature;

/**
 * Author: Steven Hansen
 * Email: steven.hansen@revature.net
 * Date: 03/15/2022 
 */

import java.util.LinkedList;

public class LinkedListDriver {

	public String intersect(LinkedList<Integer> A, LinkedList<Integer> B) {

		// Checking if both list are equal
		if (A.size() == B.size()) {
			for (int i = 0; i < A.size(); i++) {

				if (A.get(i) == B.get(i)) {
					int ans = A.get(i);
					return "The two lists intersect at the node: " + String.valueOf(i) + "\nWith a value of: "
							+ String.valueOf(ans);
				}

			}
		} else {
			return "The two list do not have equal length";
		}

		return "Nothing was tested";
	}

	public static void main(String[] args) {

		//Creating a driver for running the methods
		LinkedListDriver ld = new LinkedListDriver();

		// Creating the first LinkedList
		LinkedList<Integer> one = new LinkedList<>();
		one.add(3);
		one.add(7);
		one.add(8);
		one.add(10);

		// Creating the second LinkedList
		LinkedList<Integer> two = new LinkedList<>();
		two.add(99);
		two.add(1);
		two.add(8);
		two.add(10);

		//Running the method
		System.out.println(ld.intersect(one, two));

	}

}
