package com.revature;

import java.util.Stack;

/**
 * Author: Steven Hansen 
 * Email: steven.hansen@revature.net 
 * Date: 03/15/2022
 */

public class StackDriver {

	//Pushing objects onto a stack
	public void push(Stack<Object> stack, Object a) {
		stack.push(a);
		System.out.println("The object: " + a + " was added to the stack");
		System.out.println("The stack contains: " + stack);
	}

	//Popping off the top value
	public void pop(Stack<Object> stack) {
		// If no elements are in the stack return this error
		if (stack == null) {
			throw new NullPointerException();
		} else {
			System.out.println("The object: " + stack.peek() + " will be removed");
			stack.pop();
		}
		System.out.println("The stack now contains: " + stack);
	}

	//Look at the maximum value in the stack
	public void max(Stack<Object> stack) {
		//If no elements are in the stack return this error
		if (stack == null) {
			throw new NullPointerException();
		} else {

			//Creating stacks to store values
			Stack<Character> letters = new Stack<Character>();
			Stack<Integer> numbers = new Stack<Integer>();

			//Organizing the main stack
			for (Object temp : stack) {
				if (temp instanceof Character) {
					letters.push((Character) temp);
				} else if (temp instanceof Integer) {
					numbers.push((Integer) temp);
				}
			}

			//Sorting the letter stack
			char maxChar = letters.peek();
			for (int i = 0; i < letters.size(); i++) {
				if (letters.get(i) > maxChar) {
					maxChar = letters.get(i);
				}
			}
			
			//Sorting the number stack
			int maxInt = numbers.peek();
			for (int i = 0; i < numbers.size(); i++) {
				if (numbers.get(i) > maxInt) {
					maxInt = numbers.get(i);
				}
			}
			
			System.out.println("The maximum number value in the stack is: "+ maxInt);
			System.out.println("The maximum character value in the stack is: "+ maxChar);
			
		}
	}

	public static void main(String[] args) {

		// Creating a driver for running the methods
		StackDriver sd = new StackDriver();

		// Creating a stack
		Stack<Object> stack = new Stack<Object>();

		
		sd.push(stack, 'v');
		sd.push(stack, 'a');
		sd.push(stack, 1);
		sd.max(stack);
		sd.pop(stack);
	}

}
