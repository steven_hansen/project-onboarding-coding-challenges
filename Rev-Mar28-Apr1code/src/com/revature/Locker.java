package com.revature;

/**
 * Author: Steven Hansen
 * Email: steven.hansen@revature.net
 * Date: 03/29/2022 
 */

public class Locker {
	
	private int lockerNum;
	
	private boolean isOpen;
	
	public Locker() {
		// TODO Auto-generated constructor stub
	}

	public Locker(int lockerNum, boolean isOpen) {
		super();
		this.lockerNum = lockerNum;
		this.isOpen = isOpen;
	}

	public int getLockerNum() {
		return lockerNum;
	}

	public void setLockerNum(int lockerNum) {
		this.lockerNum = lockerNum;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	@Override
	public String toString() {
		return "Locker [lockerNum=" + lockerNum + ", isOpen=" + isOpen + "]";
	}
	
	

}
