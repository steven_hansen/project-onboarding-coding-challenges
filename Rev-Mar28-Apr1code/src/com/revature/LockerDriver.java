package com.revature;

/**
 * Author: Steven Hansen
 * Email: steven.hansen@revature.net
 * Date: 03/29/2022 
 */

import java.util.ArrayList;
import java.util.List;

public class LockerDriver {
	
	public static List<Locker> setUp(List<Locker> lockers){
		for (int i=1; i<101; i++) {
			Locker locker = new Locker(i, false);
			lockers.add(locker);
		}
		return lockers;
	}
	
	public static Locker toggleOpen(Locker a) {
		if(a.isOpen()) {
			a.setOpen(false);
		}
		else {
			a.setOpen(true);
		}
		return a;
	}

	public static void main(String[] args) {
		
		List<Locker> hallwayLockers = new ArrayList<>();
		setUp(hallwayLockers);
		
		//first pass
		for(Locker temp: hallwayLockers) {
			toggleOpen(temp);
		}
		
		//second pass
		for(Locker temp: hallwayLockers) {
			if((temp.getLockerNum()%2)==0)
			toggleOpen(temp);
		}
		
		int i = 3;
		//third and ith-100th pass
		for(Locker temp: hallwayLockers) {
			i++;
			if((temp.getLockerNum()%3)==0) {
				toggleOpen(temp);
			}
			else if((temp.getLockerNum()%i)==0) {
				toggleOpen(temp);
			}
		}
		
		//final result
		for(Locker temp: hallwayLockers) {
			if(temp.isOpen())
				System.out.println("The "+temp);
		}
		
		
	}

}
